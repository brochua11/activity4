//Axel Brochu 2233104

package linearalgebra;

public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public double magnitude(){
        double magnitude = this.x * this.x + this.y * this.y + this.z * this.z;
        return Math.sqrt(magnitude);
    }

    public double dotProduct(Vector3d v){
        return v.getX() * this.x + v.getY() * this.y + v.getZ() * this.z;
    }

    public Vector3d add(Vector3d v){
        double x = v.getX() + this.x;
        double y = v.getY() + this.y;
        double z = v.getZ() + this.z;
        return new Vector3d(x, y, z); 
    }

    public String toString(){
        return "(" + this.x + ", " + this.y + ", " + this.z + ")";
    }
}
