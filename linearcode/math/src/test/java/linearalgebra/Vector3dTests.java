//Axel Brochu 2233104
package linearalgebra;

import static org.junit.Assert.*;

import org.junit.Test;

public class Vector3dTests {
    @Test
    public void getXReturns3(){
        Vector3d v = new Vector3d(3, 5, 8);
        assertEquals(3.0, v.getX(),  0.1);
    }

    @Test
    public void getYReturns5(){
        Vector3d v = new Vector3d(3, 5, 8);
        assertEquals(5.0, v.getY(),  0.1);
    }

    @Test
    public void getZReturns8(){
        Vector3d v = new Vector3d(3, 5, 8);
        assertEquals(8.0, v.getZ(),  0.1);
    }

    @Test
    public void magnitudeReturns12_12(){
        Vector3d v = new Vector3d(7, 7, 7);
        assertEquals(12.12435, v.magnitude(), 0.001);
    }

    @Test
    public void dotProductReturns40(){
        Vector3d v = new Vector3d(4, 5, 2);
        Vector3d v2 = new Vector3d(6, 2, 3);
        assertEquals(40, v.dotProduct(v2), 0.001);
    }

    @Test
    public void addReturns10_7_5(){
        Vector3d v = new Vector3d(4, 5, 2);
        Vector3d v2 = new Vector3d(6, 2, 3);

        Vector3d sum = v.add(v2);

        Vector3d expected = new Vector3d(10, 7, 5);

        assertEquals(expected.getX(), sum.getX(), 0.01);
        assertEquals(expected.getY(), sum.getY(), 0.01);
        assertEquals(expected.getZ(), sum.getZ(), 0.01);
    }
}
